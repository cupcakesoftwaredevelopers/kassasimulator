package nl.ocarina.kassasysteem;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.google.zxing.client.android.Intents;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.json.JSONException;
import org.json.JSONObject;

import nl.ocarina.kassasysteem.api.Api;

public class ScanActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        initButton();
    }

    private void initButton() {
        Button b = (Button) findViewById(R.id.scanButton);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new IntentIntegrator(ScanActivity.this).initiateScan();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result.getContents() == null) {
            Log.d("MainActivity", "Cancelled scan");
            return;
        }
        try {
            Log.d("json", result.getContents());
            Api.getInstance(this).confirmPayment(new JSONObject(result.getContents()), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d("Response", response.toString());
                    Drawable d = null;
                    try {
                        if (response.getBoolean("payment_succeeded")) {
                            d = ContextCompat.getDrawable(ScanActivity.this, R.drawable.ic_check);
                            d.mutate().setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY);
                        } else {
                            d = ContextCompat.getDrawable(ScanActivity.this, R.drawable.ic_close);
                            d.mutate().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
                        }
                    } catch (JSONException e) {}
                    ((ImageView) findViewById(R.id.succeeded_or_failed)).setImageDrawable(d);
                }
            }, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
