package nl.ocarina.kassasysteem.api;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by emmap on 26-1-2016.
 */
public class Api {

    private static final String SERVER_HOST = "http://www.snowboom.nu:8123/";
    private static final String ROUTE_CONFIRM_PAYMENT = "basket/confirm_payment";

    private static Api instance = null;
    private RequestQueue queue;

    private Api(Context context) {
        if (queue == null) {
            queue = Volley.newRequestQueue(context);
        }
    }

    public static Api getInstance(Context context) {
        if (instance == null) {
            instance = new Api(context);
        }
        return instance;
    }

    public void doRequest(String route, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener, JSONObject jsonObject) {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, SERVER_HOST + route, jsonObject, responseListener, errorListener);
        queue.add(request);
    }

    public void confirmPayment(JSONObject userCredentials, Response.Listener<JSONObject> responseListener, Response.ErrorListener errorListener) {
        doRequest(ROUTE_CONFIRM_PAYMENT, responseListener, errorListener, userCredentials);
    }

}
